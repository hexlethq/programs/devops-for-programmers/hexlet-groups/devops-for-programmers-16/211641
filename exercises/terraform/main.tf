terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
}
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "victory" {
  name = "victory"
}

resource "digitalocean_droplet" "web" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
}

resource "digitalocean_droplet" "web1" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    path     = "/"
    protocol = "http"
  }

  droplet_ids = [digitalocean_droplet.web.id,digitalocean_droplet.web1.id]
}

resource "digitalocean_domain" "default" {
  name       = "tf.gitpushforce.club"
  ip_address = digitalocean_loadbalancer.public.ip
}

output "droplets_ip_addr" {
  value = [
      digitalocean_droplet.web.ipv4_address,
      digitalocean_droplet.web1.ipv4_address
    ]
}