variable "all_ip" {
  type = string
  default = "0.0.0.0/0"
}

resource "digitalocean_firewall" "bastion" {
  name = "bastion-firewall"

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.all_ip]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = [var.all_ip]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "22"
    destination_addresses = [digitalocean_vpc.security.ip_range]
  }
}

resource "digitalocean_firewall" "web" {
  name = "web-firewall"

  droplet_ids = [
    digitalocean_droplet.security1.id,
    digitalocean_droplet.security2.id
  ]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [digitalocean_droplet.bastion.ipv4_address_private]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "5000"
    source_addresses = [digitalocean_vpc.security.ip_range]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = [digitalocean_vpc.security.ip_range]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "80"
    destination_addresses = [var.all_ip]
  }
  outbound_rule {
    protocol              = "tcp"
    port_range            = "53"
    destination_addresses = [var.all_ip]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = [var.all_ip]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "443"
    destination_addresses = [var.all_ip]
  }
}