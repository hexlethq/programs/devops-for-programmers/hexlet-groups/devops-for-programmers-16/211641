terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "victory" {
  name = "victory"
}

resource "digitalocean_domain" "default" {
  name       = "sec.gitpushforce.club"
  ip_address = digitalocean_loadbalancer.security.ip
}

output "droplets_ip_addr" {
  value = [
    digitalocean_droplet.security1.ipv4_address_private,
    digitalocean_droplet.security2.ipv4_address_private
  ]
}