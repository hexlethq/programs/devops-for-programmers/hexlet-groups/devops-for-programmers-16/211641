resource "digitalocean_vpc" "security" {
  name     = "security-network"
  region   = "ams3"
  ip_range = "192.168.1.0/24"
}

resource "digitalocean_droplet" "bastion" {
  image  = "ubuntu-20-10-x64"
  name   = "bastion-security"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
  vpc_uuid = digitalocean_vpc.security.id
}

resource "digitalocean_droplet" "security1" {
  image  = "docker-20-04"
  name   = "security-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
  vpc_uuid = digitalocean_vpc.security.id
}

resource "digitalocean_droplet" "security2" {
  image  = "docker-20-04"
  name   = "security-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
  vpc_uuid = digitalocean_vpc.security.id
}

resource "digitalocean_certificate" "ssl" {
  name    = "ssl-certificate"
  type    = "lets_encrypt"
  domains = ["sec.gitpushforce.club"]
}

resource "digitalocean_loadbalancer" "security" {
  name   = "loadbalancer-security"
  region = "ams3"
  vpc_uuid = digitalocean_vpc.security.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"

    certificate_name = digitalocean_certificate.ssl.name
  }

  healthcheck {
    port     = 5000
    path     = "/"
    protocol = "http"
  }

  droplet_ids = [digitalocean_droplet.security1.id,digitalocean_droplet.security2.id]
}