terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  type = string
  sensitive = true
}
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "victory" {
  name = "victory"
}

resource "digitalocean_droplet" "afs1" {
  image  = "ubuntu-20-10-x64"
  name   = "web-ansible-homework-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
}

resource "digitalocean_droplet" "afs2" {
  image  = "ubuntu-20-10-x64"
  name   = "web-ansible-homework-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.victory.id]
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-afs"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    path     = "/"
    protocol = "http"
  }

  droplet_ids = [digitalocean_droplet.afs1.id,digitalocean_droplet.afs2.id]
}

resource "digitalocean_domain" "default" {
  name       = "afs.gitpushforce.club"
  ip_address = digitalocean_loadbalancer.public.ip
}

output "droplets" {
  value = [
      digitalocean_droplet.afs1,
      digitalocean_droplet.afs2
    ]
}

output "ssh_key" {
  value = data.digitalocean_ssh_key.victory.public_key
}